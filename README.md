# ROSS
Remote Onion Site Script

This script allows you to make a GET request against an .onion URL.
It retrieves the HTML source code to being analyzed later.

### Usage

Just type (as superuser):

```
python ross.py
```

and a prompt will ask for the URL.
